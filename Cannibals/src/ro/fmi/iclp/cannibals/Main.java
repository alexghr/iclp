package ro.fmi.iclp.cannibals;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        Random random = new Random();

        int max = random.nextInt(10) + 1; // don't allow 0
        Pot pot = new Pot(max, random.nextInt(max));
        Cook cook = new Cook(pot, random.nextInt(10));

        int cannibalCount = random.nextInt(10) + 1;
        List<Cannibal> cannibals = new ArrayList<>();
        for (int i = 0; i < cannibalCount; ++i) {
            cannibals.add(new Cannibal(pot, cook, random.nextInt(500), random.nextInt(10)));
        }

        System.out.printf("Pot: max=%d, current=%d\n", pot.getMaxMissionaries(), pot.getCurrentMissionaries());
        System.out.printf("Cannibals: count=%d\n", cannibalCount);

        cannibals.stream()
                .forEach((cannibal) -> System.out.printf("%s: delay=%d\n", cannibal.toString(), cannibal.getDelay()));

        System.out.println("---------------------------");

        Thread cookThread = new Thread(cook);
        cookThread.setDaemon(true);
        cookThread.start();

        cannibals.stream()
                 .map(Thread::new)
                 .forEach(Thread::start);
    }

}
