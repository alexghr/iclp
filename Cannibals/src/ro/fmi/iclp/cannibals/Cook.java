package ro.fmi.iclp.cannibals;

import java.util.Random;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Cook implements Runnable {

    private Pot pot;
    private int fillTime;

    private Lock lock = new ReentrantLock();
    private Condition emptyCondition = lock.newCondition();
    private boolean requestedFill = false;

    public Cook(Pot pot, int fillTime) {
        this.pot = pot;
        this.fillTime = fillTime;
    }

    public int getFillTime() {
        return fillTime;
    }

    public synchronized void notifyCook(Cannibal cannibal) {
        if (requestedFill) {
            return;
        }

        lock.lock();
        try {
            System.out.println(cannibal + " told the cook to fill the pot");
            requestedFill = true;
            emptyCondition.signal();
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void run() {
        while (true) {
            lock.lock();
            try {
                while (!requestedFill) {
                    emptyCondition.await();
                }
                pot.fillPot(this);
                requestedFill = false;
            } catch (InterruptedException e) {
                System.err.println(e);
            } finally {
                lock.unlock();
            }
        }
    }

    @Override
    public String toString() {
        return "Cook";
    }
}
