package ro.fmi.iclp.cannibals;

public class Cannibal implements Runnable {

    private static int ID_COUNTER = 0;

    private Pot pot;
    private Cook cook;
    private int delay, eatTime;
    private int id = ++ID_COUNTER;

    public Cannibal(Pot pot, Cook cook, int delayToEat, int eatDuration) {
        this.pot = pot;
        this.cook = cook;
        this.delay = delayToEat;
        this.eatTime = eatDuration;
    }

    public int getDelay() {
        return delay;
    }

    public int getEatTime() {
        return eatTime;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(delay);
        } catch (InterruptedException e) {
            System.err.println(e);
        }

        try {
            pot.serveMissionary(this, cook);
        } catch (InterruptedException e) {
            System.err.println(e);
        }
    }

    @Override
    public String toString() {
        return "Cannibal{" + id + '}';
    }
}
