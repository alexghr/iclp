package ro.fmi.iclp.cannibals;

import java.util.concurrent.locks.*;

public class Pot {

    private int maxMissionaries;
    private int currentMissionaries;

    private Lock lock = new ReentrantLock();
    private Condition notEmpty = lock.newCondition();

    public Pot(int maxMissionaries, int currentMissionaries) {
        this.maxMissionaries = maxMissionaries;
        this.currentMissionaries = currentMissionaries;
    }

    public int getMaxMissionaries() {
        return maxMissionaries;
    }

    public int getCurrentMissionaries() {
        return currentMissionaries;
    }

    public void serveMissionary(Cannibal cannibal, Cook cook) throws InterruptedException {
        lock.lock();
        try {
            while (currentMissionaries == 0) {
                cook.notifyCook(cannibal);
                notEmpty.await();
            }

            Thread.sleep(cannibal.getEatTime());

            --currentMissionaries;
            System.out.println(cannibal + " ate a missionary! (" + getCurrentMissionaries() + " left)");
        } finally {
            lock.unlock();
        }
    }

    public void fillPot(Cook cook) throws InterruptedException {
        lock.lock();
        try {
            Thread.sleep(cook.getFillTime());

            currentMissionaries = maxMissionaries;
            System.out.println(cook + " filled the pot!");

            notEmpty.signalAll();
        } finally {
            lock.unlock();
        }
    }
}
