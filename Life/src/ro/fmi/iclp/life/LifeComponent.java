package ro.fmi.iclp.life;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LifeComponent extends Canvas implements Runnable {

    private static final int FPS = 5;

    private static final int ALIVE_COLOR = Color.WHITE.getRGB();
    private static final int DEAD_COLOR = Color.BLACK.getRGB();

    private BufferedImage screenImage;
    private int[] screenPixels;

    private int canvasWidth, canvasHeight;
    private int gridWidth, gridHeight;

    private int xScale, yScale;

    private Life life;

    private boolean running = false;

    private Thread myThread;

    private ExecutorService cellExecutor = Executors.newCachedThreadPool();

    private Lock computeGenerationLock = new ReentrantLock();
    private Lock drawLock = new ReentrantLock();

    public LifeComponent(int canvasWidth, int canvasHeight, Life life) {
        this.canvasWidth = canvasWidth;
        this.canvasHeight = canvasHeight;

        this.life = life;
        gridWidth = life.getGridWidth();
        gridHeight = life.getGridHeight();

        screenImage = new BufferedImage(gridWidth, gridHeight, BufferedImage.TYPE_INT_RGB);
        screenPixels = ((DataBufferInt) screenImage.getRaster().getDataBuffer()).getData();

        this.setSizes();
        this.computeScale();
    }

    private void setSizes() {
        Dimension size = new Dimension(canvasWidth, canvasHeight);

        setSize(size);
        setPreferredSize(size);
        setMinimumSize(size);
        setMaximumSize(size);
    }

    private void computeScale() {
        xScale = canvasWidth / gridWidth;
        yScale = canvasHeight / gridHeight;

        if (xScale == 0 || yScale == 0) {
            throw new RuntimeException("Life grid too big for this component");
        }
    }

    private void render() {
        BufferStrategy bufferStrategy = getBufferStrategy();
        if (bufferStrategy == null) {
            createBufferStrategy(2);
            return;
        }

        drawLock.lock();
        life.forEach(
                (cell) ->
                        screenPixels[cell.getY() * gridWidth + cell.getX()] = cell.isAlive() ? ALIVE_COLOR : DEAD_COLOR
        );
        drawLock.unlock();

        Graphics graphics = bufferStrategy.getDrawGraphics();
        graphics.clearRect(0, 0, canvasWidth, canvasHeight);
        graphics.drawImage(screenImage, 0, 0, gridWidth * xScale, gridHeight * yScale, null);
        graphics.dispose();
        bufferStrategy.show();
    }

    public void start() {
        if (running) {
            return;
        }

        running = true;
        myThread = new Thread(this);
        myThread.start();
    }

    public void stop() {
        if (!running) {
            return;
        }

        running = false;
        try {
            myThread.join();
        } catch (InterruptedException e) {
            System.err.println(e);
        }
    }

    @Override
    public void run() {
        long lastTime, now, deltaTime;
        lastTime = System.nanoTime();

        double secondsPerTick = 1.0 / FPS;
        double unprocessedSeconds = 0;

        boolean ticked = true;
        while (running) {
            now = System.nanoTime();
            deltaTime = now - lastTime;
            lastTime = now;

            if (deltaTime < 0) {
                deltaTime = 0;
            }

            unprocessedSeconds += deltaTime * 1.0e-9;

            while (unprocessedSeconds >= secondsPerTick) {
                tick();
                ticked = true;
                unprocessedSeconds -= secondsPerTick;
            }

            if (ticked) {
                render();
                ticked = false;
            } else {
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    System.err.println(e);
                }
            }
        }
    }

    private void tick() {
        if (!computeGenerationLock.tryLock()) {
            System.out.println("Already computing!");
            return;
        }

        try {
            Worker[] workers = life.stream().map(
                    Worker::new
            ).toArray(Worker[]::new);

            List<Future<Worker.Result>> futures = cellExecutor.invokeAll(Arrays.asList(workers));

            List<Worker.Result> results = new ArrayList<>();
            for (Future<Worker.Result> future : futures) {
                try {
                    results.add(future.get());
                } catch (InterruptedException | ExecutionException e) {
                    System.err.println(e);
                }
            }

            boolean[] newGeneration = new boolean[gridWidth * gridHeight];
            results.stream().forEach(
                    (result) ->
                            newGeneration[result.getY() * gridWidth + result.getX()] = result.isAlive()
            );

            Life newLife = new Life(newGeneration, gridWidth);

            drawLock.lock();
            life = newLife;
            drawLock.unlock();

        } catch (InterruptedException e) {
            System.err.println(e);
        } finally {
            computeGenerationLock.unlock();
        }
    }

}
