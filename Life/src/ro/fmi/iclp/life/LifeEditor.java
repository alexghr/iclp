package ro.fmi.iclp.life;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.lang.reflect.Array;
import java.util.Arrays;

public class LifeEditor extends Canvas {

    private static enum GridCellState {
        ALIVE, DEAD, HOVERED
    }

    private class MouseHandler extends MouseAdapter {

        private int prevHoverIndex = -1;

        @Override
        public void mouseClicked(MouseEvent e) {
            int index = getIndexForMouse(e.getX(), e.getY());
            if (grid[index] == GridCellState.ALIVE) {
                grid[index] = GridCellState.HOVERED;
                prevHoverIndex = index;
            } else if (grid[index] == GridCellState.HOVERED) {
                grid[index] = GridCellState.ALIVE;
                prevHoverIndex = -1;
            }

            repaint();
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            int index = getIndexForMouse(e.getX(), e.getY());
            if (index == prevHoverIndex) {
                return;
            }

            if (prevHoverIndex != -1 && grid[prevHoverIndex] == GridCellState.HOVERED) {
                grid[prevHoverIndex] = GridCellState.DEAD;
            }

            if (grid[index] == GridCellState.DEAD) {
                grid[index] = GridCellState.HOVERED;
                prevHoverIndex = index;
            }

            repaint();
        }

        private int getIndexForMouse(int mouseX, int mouseY) {
            mouseX = mouseX < 0 ? 0 : mouseX >= canvasWidth ? canvasWidth - 1 : mouseX;
            mouseY = mouseY < 0 ? 0 : mouseY >= canvasHeight ? canvasHeight - 1 : mouseY;

            int scaledX = (int) Math.floor(mouseX / (double) xScale);
            int scaledY = (int) Math.floor(mouseY / (double) yScale);

            return scaledY * gridWidth + scaledX;
        }

        public void clear() {
            prevHoverIndex = -1;
        }
    }

    private BufferedImage screenImage;
    private int[] screenPixels;

    private int canvasWidth, canvasHeight;
    private int gridWidth, gridHeight;
    private int xScale, yScale;

    private GridCellState[] grid;

    MouseHandler mouseHandler = new MouseHandler();

    public LifeEditor(int canvasWidth, int canvasHeight, int gridWidth, int gridHeight) {
        this.canvasWidth = canvasWidth;
        this.canvasHeight = canvasHeight;
        this.gridWidth = gridWidth;
        this.gridHeight = gridHeight;

        this.setSizes();

        recreateScreen();


        addMouseMotionListener(mouseHandler);
        addMouseListener(mouseHandler);
    }

    public Boolean[] getMap() {
        return Arrays.stream(grid).map(
                (state) -> state == GridCellState.ALIVE
        ).toArray(Boolean[]::new);
    }

    public int getGridWidth() {
        return gridWidth;
    }

    public int getGridHeight() {
        return gridHeight;
    }

    public void setGridWidth(int newWidth) {
        gridWidth = newWidth;
        recreateScreen();
    }

    public void setGridHeight(int newHeight) {
        gridHeight = newHeight;
        recreateScreen();
    }

    private void recreateScreen() {
        grid = new GridCellState[gridHeight * gridWidth];
        Arrays.fill(grid, GridCellState.DEAD);

        computeScale();

        screenImage = new BufferedImage(gridWidth, gridHeight, BufferedImage.TYPE_INT_RGB);
        screenPixels = ((DataBufferInt) screenImage.getRaster().getDataBuffer()).getData();

        mouseHandler.clear();
    }

    private void setSizes() {
        Dimension size = new Dimension(canvasWidth, canvasHeight);

        setSize(size);
        setPreferredSize(size);
        setMinimumSize(size);
        setMaximumSize(size);
    }

    private void computeScale() {
        xScale = canvasWidth / gridWidth;
        yScale = canvasHeight / gridHeight;

        if (xScale == 0 || yScale == 0) {
            throw new RuntimeException("Life grid too big for this component");
        }
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);

        for (int i = 0, len = grid.length; i < len; ++i) {
            screenPixels[i] = getColorForState(grid[i]);
        }

        g.drawImage(screenImage, 0, 0, gridWidth * xScale, gridHeight * yScale, null);
    }

    private int getColorForState(GridCellState state) {
        switch (state) {
            case ALIVE:
                return Color.WHITE.getRGB();

            case DEAD:
                return Color.BLACK.getRGB();

            case HOVERED:
                return Color.BLUE.getRGB();

            default:
                throw new RuntimeException();
        }
    }
}
