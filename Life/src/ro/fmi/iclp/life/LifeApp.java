package ro.fmi.iclp.life;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;

public class LifeApp extends JFrame {

    private static int WIDTH = 500;
    private static int HEIGHT = 500;

    private Life life;
    private LifeComponent game;
    private LifeEditor editor;

    private JPanel content;
    private JPanel bottom;

    private JSpinner widthSpinner, heightSpinner;
    private JLabel widthLabel, heightLabel;
    private JButton startBtn;

    private JButton stopBtn;

    @Override
    protected void frameInit() {
        super.frameInit();

        bottom = new JPanel(new BorderLayout());
        bottom.setMinimumSize(new Dimension(WIDTH, 100));

        content = new JPanel(new BorderLayout());
        content.add(bottom, BorderLayout.SOUTH);

        setContentPane(content);
        setLocationRelativeTo(null);
        setResizable(false);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);

        this.showEditor();
    }

    private void showEditor() {
        if (game != null) {
            game.stop();
            content.remove(game);
            game = null;
        }

        bottom.removeAll();

        if (editor == null) {
            editor = new LifeEditor(WIDTH, HEIGHT, 50, 50);

            SpinnerNumberModel widthModel = new SpinnerNumberModel(50, 25, 250, 1);
            SpinnerNumberModel heightModel = new SpinnerNumberModel(50, 25, 250, 1);

            widthSpinner = new JSpinner(widthModel);
            heightSpinner = new JSpinner(heightModel);
            widthLabel = new JLabel("Width");
            heightLabel = new JLabel("Height");
            startBtn = new JButton("Start");

            widthSpinner.addChangeListener((e) -> editor.setGridWidth(widthModel.getNumber().intValue()));
            heightSpinner.addChangeListener((e) -> editor.setGridHeight(heightModel.getNumber().intValue()));
            startBtn.addActionListener((e) -> this.startAnimation());
        }

        JPanel width = new JPanel(new FlowLayout());
        width.add(widthLabel);
        width.add(widthSpinner);

        JPanel height = new JPanel(new FlowLayout());
        height.add(heightLabel);
        height.add(heightSpinner);

        JPanel centered = new JPanel(new FlowLayout());
        centered.add(width);
        centered.add(height);

        bottom.add(centered, BorderLayout.CENTER);
        bottom.add(startBtn, BorderLayout.SOUTH);

        content.add(editor, BorderLayout.CENTER);

        pack();
    }

    private void startAnimation() {
        content.remove(editor);
        bottom.removeAll();

        Boolean[] editorMap = editor.getMap();
        boolean[] map = new boolean[editorMap.length];
        for (int i = 0; i < editorMap.length; ++i) {
            map[i] = editorMap[i];
        }

        life = new Life(map, editor.getGridWidth());
        game = new LifeComponent(WIDTH, HEIGHT, life);

        if (stopBtn == null) {
            stopBtn = new JButton("Stop");
            stopBtn.addActionListener((e) -> this.showEditor());
        }

        content.add(game, BorderLayout.CENTER);
        bottom.add(stopBtn, BorderLayout.CENTER);

        game.start();

        pack();
    }
}
