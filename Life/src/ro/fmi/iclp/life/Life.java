package ro.fmi.iclp.life;

import java.util.*;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class Life implements Iterable<Life.Cell> {

    public class Cell {

        private final boolean alive;
        private final int x, y;

        private Cell(boolean isAlive, int x, int y) {
            this.alive = isAlive;
            this.x = x;
            this.y = y;
        }

        public boolean isAlive() {
            return alive;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }

        public Cell bottom() {
            return getCellAt(x, y + 1);
        }

        public Cell top() {
            return getCellAt(x, y - 1);
        }

        public Cell left() {
            return getCellAt(x - 1, y);
        }

        public Cell right() {
            return getCellAt(x + 1, y);
        }

        private Cell[] getNeighbours() {
            Cell[] neighbours = new Cell[8];
            // neighbours starting from top and in clockwise order
            neighbours[0] = top();
            neighbours[1] = neighbours[0].right();
            neighbours[2] = right();
            neighbours[3] = neighbours[2].bottom();
            neighbours[4] = bottom();
            neighbours[5] = neighbours[4].left();
            neighbours[6] = left();
            neighbours[7] = neighbours[6].top();

            return neighbours;
        }

        private int countAliveNeighbours() {
            return (int) Arrays.stream(getNeighbours())
                         .filter((cell) -> cell.alive)
                         .count();
        }

        public boolean aliveNextGeneration() {
            int aliveNeighbours = countAliveNeighbours();
            if (aliveNeighbours < 2 || aliveNeighbours > 3) {
                return false;
            } else if (aliveNeighbours == 3 && !alive) {
                return true;
            }

            return alive;
        }
    }

    private final List<Cell> gridList;
    private final int gridWidth, gridHeight;

    private Life(int gridWidth, int gridHeight, boolean initEmpty) {
        this.gridWidth = gridWidth;
        this.gridHeight = gridHeight;

        gridList = new ArrayList<>(gridWidth * gridHeight);

        if (initEmpty) {
            initGrid();
        }
    }

    public Life(int xSize, int ySize) {
        this(xSize, ySize, true);
    }

    public Life(boolean[][] cells) {
        this(cells[0].length, cells.length, false);
        initGrid(cells);
    }

    public Life(boolean[] cells, int gridWidth) {
        this(gridWidth, cells.length / gridWidth, false);
        initGrid(cells);
    }

    public int getGridHeight() {
        return gridHeight;
    }

    public int getGridWidth() {
        return gridWidth;
    }

    public Cell getCellAt(int x, int y) {
        y = Math.floorMod(y, gridHeight);
        x = Math.floorMod(x, gridWidth);

        return gridList.get(y * gridWidth + x);
    }

    public Life nextGeneration() {
        final boolean[][] nextGeneration = new boolean[getGridHeight()][getGridWidth()];
        forEach((cell) -> nextGeneration[cell.y][cell.x] = cell.aliveNextGeneration());

        return new Life(nextGeneration);
    }

    @Override
    public Iterator<Cell> iterator() {
        return gridList.iterator();
    }

    public Stream<Cell> stream() {
        return gridList.stream();
    }

    private void initGrid() {
        for (int i = 0, len = gridHeight * gridWidth; i < len; ++i) {
            gridList.add(new Cell(false, i % gridWidth, i / gridWidth));
        }
    }

    private void initGrid(boolean[][] cells) {
        for (int i = 0, height = cells.length; i < height; ++i) {
            for (int j = 0, width = cells[i].length; j < width; ++j) {
                gridList.add(new Cell(cells[i][j], j, i));
            }
        }
    }

    private void initGrid(boolean[] cells) {
        for (int i = 0; i < gridHeight; ++i) {
            for (int j = 0; j < gridWidth; ++j) {
                gridList.add(new Cell(cells[i * gridWidth + j], j, i));
            }
        }
    }
}
