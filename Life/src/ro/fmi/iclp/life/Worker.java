package ro.fmi.iclp.life;

import java.util.concurrent.Callable;

public class Worker implements Callable<Worker.Result> {

    public static class Result {
        private final int x, y;
        private final boolean alive;

        public Result(boolean alive, int x, int y) {
            this.x = x;
            this.y = y;
            this.alive = alive;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }

        public boolean isAlive() {
            return alive;
        }
    }

    private final Life.Cell cell;

    public Worker(Life.Cell cell) {
        this.cell = cell;
    }

    @Override
    public Result call() throws Exception {
        boolean alive = cell.aliveNextGeneration();
        return new Result(alive, cell.getX(), cell.getY());
    }
}
