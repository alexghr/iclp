package ro.fmi.iclp.cards;

import java.io.*;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Main implements Runnable {

    private ExecutorService workerExecutor = Executors.newCachedThreadPool();

    private Reader reader;
    private Writer writer;

    public Main(InputStream stream ) throws FileNotFoundException {
        reader = new Reader(new Scanner(stream), this::onRead);
        writer = new Writer(new PrintWriter(System.out, true));
    }

    @Override
    public void run() {
        Thread readThread = new Thread(reader);
        Thread writeThread = new Thread(writer);

        readThread.start();
        writeThread.start();

        try {
            readThread.join();
        } catch (InterruptedException e) {
            System.err.println(e);
        }

        workerExecutor.shutdown();
        do {
            try {
                workerExecutor.awaitTermination(10, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
                System.err.println(e);
            }
        } while (!workerExecutor.isTerminated());

        writer.stop();
    }

    private void onRead(String line) {
        workerExecutor.submit(new Worker(line, this::onProcess));
    }

    private void onProcess(String processed) {
        writer.scheduleWriteln(processed);
    }

    public static void main(String[] args) {
        boolean useSystemIn = false;

        try {
            new Main(useSystemIn ? System.in : new FileInputStream("res/cards/in.txt")).run();
        } catch (FileNotFoundException e) {
            System.err.println(e);
        }
    }
}
