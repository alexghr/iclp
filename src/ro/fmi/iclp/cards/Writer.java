package ro.fmi.iclp.cards;

import java.io.PrintWriter;
import java.util.Objects;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;

public class Writer implements Runnable {

    private BlockingDeque<String> lineQueue = new LinkedBlockingDeque<>();

    private boolean stopped = false;

    private PrintWriter writer;

    public Writer(PrintWriter writer) {
        this.writer = Objects.requireNonNull(writer);
    }

    public synchronized void scheduleWriteln(String line) {
        lineQueue.addLast(line);
    }

    public synchronized void stop() {
        stopped = true;
    }

    @Override
    public void run() {
        while (!stopped || lineQueue.peekFirst() != null) {
            try {
                String line = lineQueue.pollFirst(10, TimeUnit.MILLISECONDS);
                if (line != null) {
                    writer.println(line);
                }
            } catch (InterruptedException e) {
                System.err.println(e);
            }

            Thread.yield();
        }
    }
}
