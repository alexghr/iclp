package ro.fmi.iclp.cards;


import java.util.Objects;
import java.util.Scanner;

public class Reader implements Runnable {

    public static interface ReadListener {
        void onCard(String card);
    }

    private Scanner input;
    private ReadListener listener;

    public Reader(Scanner input, ReadListener listener) {
        this.input = Objects.requireNonNull(input);
        this.listener = Objects.requireNonNull(listener);
    }

    @Override
    public void run() {
        while (input.hasNextLine()) {
            listener.onCard(input.nextLine() + '$');
            Thread.yield();
        }
    }
}
