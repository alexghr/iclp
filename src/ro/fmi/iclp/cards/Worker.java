package ro.fmi.iclp.cards;

import java.util.Objects;

public class Worker implements Runnable {

    public static interface WorkListener {
        void onDone(String processed);
    }

    private String card;
    private WorkListener listener;

    public Worker(String card, WorkListener listener) {
        this.card = Objects.requireNonNull(card);
        this.listener = Objects.requireNonNull(listener);
    }

    @Override
    public void run() {
        listener.onDone(card.replace("**", "!"));
    }
}
